///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown headers
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Emily Pham <emilyn3@hawaii.edu>
// @date   TODO_04_FEB_2021
///////////////////////////////////////////////////////////////////////////////

//@note THIS COUNTDOWN DOES NOT ACCOUNT FOR LEAP YEARS/DAYS/SECONDS

#define MAR 2
#define FRI 5

#define SEC_PER_YEAR 31536000
#define SEC_PER_DAY 86400
#define SEC_PER_HOUR 3600
#define SEC_PER_MIN 60

void timeSinceRef ( struct tm *timeSince, struct tm *refTime );


