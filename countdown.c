///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Emily Pham <emilyn3@hawaii.edu>
// @date   TODO_04_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include<time.h>
#include<unistd.h>
#include"countdown.h"

int main() {

   struct tm refTime = {0};
      refTime.tm_mday = 20;
      refTime.tm_mon = MAR;
      refTime.tm_year = 2020 - 1900;
      refTime.tm_wday = FRI;

    struct tm timeSince = {0};
  
   printf("Reference time: %s\n", asctime(&refTime) );

   while( 1 ) {
      timeSinceRef( &timeSince, &refTime );
      printf("Years: %d  Days: %d  Hours: %d  Minutes: %d  Seconds: %d \n",
               timeSince.tm_year, timeSince.tm_yday, timeSince.tm_hour, timeSince.tm_min, timeSince.tm_sec );
      sleep(1);
   }

   return 0;
}


void timeSinceRef ( struct tm *timeSince, struct tm *refTime ){
// I know this isn't the cleanest but I ran out of time.

   double timeDiffSeconds = 0;
   time_t timeNow;
   timeNow = time(NULL);
   float remainder;
   timeDiffSeconds = difftime( timeNow, mktime(refTime) );

   remainder = (int)timeDiffSeconds % SEC_PER_YEAR;
   timeSince->tm_year = (timeDiffSeconds - remainder) / SEC_PER_YEAR;

   remainder = (int)timeDiffSeconds % SEC_PER_DAY;
   timeSince->tm_yday = ( (timeDiffSeconds - remainder) - (timeSince->tm_year * SEC_PER_YEAR) ) / SEC_PER_DAY;

   remainder = (int)timeDiffSeconds % SEC_PER_HOUR;
   timeSince->tm_hour = ( (timeDiffSeconds - remainder) - (timeSince->tm_year * SEC_PER_YEAR) -
                     (timeSince->tm_yday * SEC_PER_DAY) ) / SEC_PER_HOUR;

   remainder = (int)timeDiffSeconds % SEC_PER_MIN;
   timeSince->tm_min = ( (timeDiffSeconds - remainder) - (timeSince->tm_year * SEC_PER_YEAR) -
                     (timeSince->tm_yday * SEC_PER_DAY) - (timeSince->tm_hour * SEC_PER_HOUR) ) / SEC_PER_MIN;

   timeSince->tm_sec = (int)timeDiffSeconds % SEC_PER_MIN;
}
